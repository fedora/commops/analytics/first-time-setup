# first-time-setup
This process sets up your machine to do basic analysis of data using Jupyter, Git, and an example CSV file.

## Clone a Git Repository
To access and work on projects hosted in a Git repository, you must first clone them:

### Install Git:
If Git is not yet installed on your system, you can install it using:
```bash
sudo dnf install git
```

#### Clone the repository:
Obtain a copy of this repository by cloning it. Replace <repository-url> with the URL of the repository you wish to clone:

```bash
git clone https://gitlab.com/fedora/commops/analytics/first-time-setup
```

### Access the repository directory:
Change to the directory of the cloned repository:

```bash
cd first-time-setup
```
## Install Jupyter

### Install Python and pip
Ensure that Python and its package installer, pip, are installed and up to date on your Fedora system:
```bash
sudo dnf install python3 python3-pip
```

### Install Jupyter
With your enviorment ready, install Jupyter using pip:
```bash
pip3 install jupyter matplotlib pandas
```

### Launch Jupyter Notebook:
Start the Jupyter Notebook server by running:
```bash
jupyter notebook
```
This command will initiate the server and open a new browser window or tab where you can create and manage your notebooks.

## Alternative - Run Jupyter in a container

For those that prefer not installing things on their host machine, Jupyter can be run in a container (via the [Jupyter/docker-stacks](https://github.com/jupyter/docker-stacks) repository). We will assume Podman here, Docker works too - after cloning and cd'ing to the directory, run:

```bash
podman run -v ./:/home/jovyan/work/:Z -p 8888:8888 quay.io/jupyter/scipy-notebook:2024-05-27
```

This starts a container (check the linked repo for the latest version), maps it to a port, and mounts the repo into it's work directory. It will print a message to the console like this:

```bash
    To access the server, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/jpserver-2-open.html
    Or copy and paste one of these URLs:
        http://7e446bb27c04:8888/lab?token=3d76868a977981385f8c3b2ccb4ba77ae050ed16ab7c99a5
        http://127.0.0.1:8888/lab?token=3d76868a977981385f8c3b2ccb4ba77ae050ed16ab7c99a5
```

The last line is what you want - paste that into your browser and JupyterLab should load.

## Run an Example Notebook
Now that you're setup, you can run any Jupyter Notebook! To start for this practice, we'll use our sample Notebook provided and a sample set of Package data.

### Navigate to the notebook file:
In this directory, locate the Jupyter Notebook file in the Notebook app (example.ipynb).
With your Jupyter Notebook server is runningy, click on the notebook file to open it in the browser.

### Execute the notebook:
Run each cell in the notebook sequentially by pressing Shift + Enter. 
This includes cells that load and process the CSV file, and the cell that generates the chart.
